package gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class Xls {
    private Workbook libro;
    
    public Xls() {
        this.libro = new HSSFWorkbook();    
    }

    public void setLibro(Workbook libro) {
        this.libro = libro;
    }

    public Workbook getLibro() {
        return libro;
    }
}

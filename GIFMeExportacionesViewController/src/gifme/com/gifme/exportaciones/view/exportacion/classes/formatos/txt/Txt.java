package gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.txt;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import oracle.adf.share.logging.ADFLogger;

import org.apache.poi.util.IOUtils;

public class Txt {
    protected ADFLogger logger = ADFLogger.createADFLogger(this.getClass());
    private TxtController txtController = new TxtController();
    
    public Txt() {
    }
    
    public void exportar(HashMap<String,String> formatoDeExportacion, LinkedHashMap<String,HashMap> mapaDeDatos) 
        throws IOException {
        File archivo = new File(formatoDeExportacion.get("nombreDeArchivo"));
        PrintStream out = new PrintStream(archivo);
        Iterator iteratorMapaDeDatos = mapaDeDatos.entrySet().iterator();
        while(iteratorMapaDeDatos.hasNext()){
            Map.Entry pair = (Map.Entry)iteratorMapaDeDatos.next();
            HashMap<Integer,String> datos = (HashMap<Integer,String>) pair.getValue();
            if (!datos.equals("null")){
                Iterator iteratorDatos = datos.entrySet().iterator();
                while (iteratorDatos.hasNext()) {
                    Map.Entry elementosObtenidos = (Map.Entry)iteratorDatos.next();
                    HashMap<String,String> valores = (HashMap<String,String>) elementosObtenidos.getValue();
                    Iterator iteradorDatos = valores.entrySet().iterator();
                    while (iteradorDatos.hasNext()){
                        Map.Entry elemento = (Map.Entry)iteradorDatos.next();
                        out.print(elemento.getValue());
                    }
                    out.println();
                }
            }
        }
        out.close();
        this.txtController.descargarArchivoTXT(archivo);
    }
    
    public void exportarZip(HashMap<String,String> formatoDeExportacion, LinkedHashMap<String,HashMap> mapaDeDatos) 
        throws IOException {
        LinkedHashMap<Integer,File> datosParaZip = new LinkedHashMap<Integer,File>();
        datosParaZip.put(1, this.almacenarDatos(formatoDeExportacion.get("nombreDeCabecera"), mapaDeDatos.get("Cabecera")));
        datosParaZip.put(2, this.almacenarDatos(formatoDeExportacion.get("nombreDeCuerpo"), mapaDeDatos.get("Cuerpo")));
        datosParaZip.put(3, this.almacenarDatos(formatoDeExportacion.get("nombreDePie"), mapaDeDatos.get("Pie")));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zipFile = new ZipOutputStream(baos);
        for(int i = 1 ;i<4;i++){ 
            zipFile.putNextEntry(new ZipEntry(datosParaZip.get(i)+""));
            InputStream fis = new FileInputStream((File) datosParaZip.get(i));
            byte[] buf = IOUtils.toByteArray(fis);
            zipFile.write(buf);;
            zipFile.closeEntry();
            fis.close();
        }
        zipFile.finish();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment; filename="+formatoDeExportacion.get("nombreDeArchivo")+".zip");
        response.getOutputStream().write(baos.toByteArray());
        response.getOutputStream().flush();
        response.getOutputStream().close();
        FacesContext.getCurrentInstance().responseComplete();
        baos.close();
        zipFile.close();
    }
        
    public File almacenarDatos(String nombreArchivo, HashMap<String,HashMap> mapaDeDatos) throws FileNotFoundException {
        File archivo = new File(nombreArchivo+".txt");
        PrintStream out = new PrintStream(archivo);
        Iterator iteratorMapaDeDatos = mapaDeDatos.entrySet().iterator();
        while(iteratorMapaDeDatos.hasNext()){
            Map.Entry pair = (Map.Entry)iteratorMapaDeDatos.next();
            HashMap<Integer,String> datos = (HashMap<Integer,String>) pair.getValue();
            if (!datos.equals("null")){
                Iterator iteratorDatos = datos.entrySet().iterator();
                while (iteratorDatos.hasNext()) {
                    Map.Entry elementosObtenidos = (Map.Entry)iteratorDatos.next();
                    out.print(elementosObtenidos.getValue());
                }
                out.println();
            }
        }
        out.close();
        return archivo;
    }

}

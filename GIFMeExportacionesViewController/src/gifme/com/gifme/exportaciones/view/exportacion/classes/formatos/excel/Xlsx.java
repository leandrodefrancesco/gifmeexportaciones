package gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.excel;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Xlsx {
    private Workbook libro;
    
    public Xlsx() {
        this.libro = new XSSFWorkbook();    
    }

    public void setLibro(Workbook libro) {
        this.libro = libro;
    }

    public Workbook getLibro() {
        return libro;
    }
}

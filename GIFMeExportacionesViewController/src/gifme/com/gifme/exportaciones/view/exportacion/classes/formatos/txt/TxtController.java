package gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.txt;

import gifme.com.gifme.exportaciones.view.exportacion.classes.controller.Exportacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.util.IOUtils;


public class TxtController {
    public TxtController() {
    }
    
    public void descargarArchivoTXT(File archivo) throws IOException{
        InputStream fis = new FileInputStream(archivo);
        byte[] buf = IOUtils.toByteArray(fis);
        int offset = 0;
        int numRead = 0;
        while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length -offset)) >= 0)){
           offset += numRead;
        }
        fis.close();
          HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
          response.setContentType("application/octet-stream");
          response.setHeader("Content-Disposition", "attachment;filename=" + archivo.getName());
          response.getOutputStream().write(buf);
          response.getOutputStream().flush();
          response.getOutputStream().close();
          FacesContext.getCurrentInstance().responseComplete();
          archivo.delete();
      }
    
}

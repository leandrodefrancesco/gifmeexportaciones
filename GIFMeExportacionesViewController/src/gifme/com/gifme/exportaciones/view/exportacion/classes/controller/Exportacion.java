package gifme.com.gifme.exportaciones.view.exportacion.classes.controller;

import gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.csv.Csv;
import gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.excel.Excel;
import gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.excel.Xls;
import gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.excel.Xlsx;
import gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.txt.Txt;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletResponse;

import oracle.adf.model.BindingContext;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;


public class Exportacion {
    protected ADFLogger logger = ADFLogger.createADFLogger(this.getClass());
    private Txt exportarTxt;
    private Excel exportarExcel;
    private Csv exportarCsv;

    public Exportacion() {
    }
    
    public void administrarExportacion(String idFormato) throws IOException{
        HashMap<String,String> formatoDeExportacion = this.obtenerFormatoDeExportacion(idFormato);
        HashMap<Integer,String> cabeceraParaExportar = this.obtenerDatosParaExportar(formatoDeExportacion.get("StoreCabecera"));
        HashMap<Integer,String> datosParaExportar = this.obtenerDatosParaExportar(formatoDeExportacion.get("StoreCuerpo"));
        HashMap<Integer,String> pieParaExportar = this.obtenerDatosParaExportar(formatoDeExportacion.get("StorePie"));
        LinkedHashMap<String,HashMap> mapaDeDatos = new LinkedHashMap<String,HashMap>();
        mapaDeDatos.put("Cabecera", cabeceraParaExportar);
        mapaDeDatos.put("Cuerpo", datosParaExportar);
        mapaDeDatos.put("Pie", pieParaExportar);
        switch(formatoDeExportacion.get("Extension")){
            case "txt":
                this.exportarTxt = new Txt();
                this.exportarTxt.exportar(formatoDeExportacion, mapaDeDatos);
                break;
            case "xls":
                Xls xls = new Xls();
                this.exportarExcel = new Excel(formatoDeExportacion, mapaDeDatos, xls.getLibro());
                this.exportarExcel.exportarExcel();
                break;
            case "xlsx":
                Xlsx xlsx = new Xlsx();
                this.exportarExcel = new Excel(formatoDeExportacion, mapaDeDatos, xlsx.getLibro());
                this.exportarExcel.exportarExcel();
                break;
            case "csv":
                this.exportarCsv = new Csv();
                this.exportarCsv.exportarCsv(formatoDeExportacion, mapaDeDatos);
                break;
            case "txtSeparados":
                this.exportarTxt = new Txt();
                this.exportarTxt.exportarZip(formatoDeExportacion, mapaDeDatos);
                break;
        }
    }
    
    private HashMap obtenerFormatoDeExportacion(String idFormato){
        OperationBinding op = this.getBinding().getOperationBinding("getFormatoExportacion");
        op.getParamsMap().put("idFormato", idFormato);
        op.execute();
        return (HashMap<String,String>) op.getResult();
    }
    
    private HashMap obtenerDatosParaExportar(String elementoParaVerificar){
        HashMap<Integer,String> elementoDeExportacion = new HashMap<Integer, String>();
        if((elementoParaVerificar == null)||(!elementoParaVerificar.equals("null"))){
            OperationBinding op = this.getBinding().getOperationBinding("getDatosParaExportacion");
            op.getParamsMap().put("StoreParaExportar", elementoParaVerificar);
            op.execute();
            elementoDeExportacion = (HashMap<Integer,String>) op.getResult();
        }
        return elementoDeExportacion;
    }
    
    private BindingContainer getBinding(){
        BindingContext bctx = new BindingContext();
        return bctx.getCurrentBindingsEntry();
    }
}

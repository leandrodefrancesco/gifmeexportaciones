package gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.excel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class Excel {
    private ExcelController excelController = new ExcelController();
    private HashMap<String,String> formatoParaExportar;
    private LinkedHashMap<String,HashMap> mapaDeDatos;
    private Workbook libro;
    
    public Excel(HashMap<String,String> formatoDeExportacion, LinkedHashMap<String,HashMap> mapaDeDatos,
                 Workbook formatoExcel) {
        this.formatoParaExportar = formatoDeExportacion;
        this.mapaDeDatos = mapaDeDatos;
        this.libro = formatoExcel;
    }
    
    public void exportarExcel() 
        throws FileNotFoundException, IOException {
        String nombreArchivo = this.formatoParaExportar.get("NombreArchivo")+ "." + this.formatoParaExportar.get("Extension");
        Sheet hoja = this.libro.createSheet("Hoja 1");
        Iterator iteratorMapaDeDatos = this.mapaDeDatos.entrySet().iterator();
        int contadorFilas = 0;
        while(iteratorMapaDeDatos.hasNext()){
            Map.Entry pair = (Map.Entry)iteratorMapaDeDatos.next();
            HashMap<Integer,String> datos = (HashMap<Integer,String>) pair.getValue();
            if (!datos.equals("null")){
                Iterator iteratorDatos = datos.entrySet().iterator();
                while (iteratorDatos.hasNext()) {
                    Row fila = hoja.createRow(contadorFilas);
                    Map.Entry elementosObtenidos = (Map.Entry)iteratorDatos.next();
                    HashMap<String,String> valores = (HashMap<String,String>) elementosObtenidos.getValue();
                    Iterator iteradorDatos = valores.entrySet().iterator();
                    int contadorColumnas = 0;
                    while (iteradorDatos.hasNext()) {
                        Cell cell = fila.createCell(contadorColumnas);
                        Map.Entry elemento = (Map.Entry)iteradorDatos.next();
                        if (elemento.getValue() instanceof String) {
                            cell.setCellValue((String) elemento.getValue());
                        }else if (elemento.getValue() instanceof Integer) {
                            cell.setCellValue((Integer) elemento.getValue());
                        }
                        contadorColumnas++;
                    }
                    contadorFilas++;
                }
            }
        }
        OutputStream outputStream = new FileOutputStream(nombreArchivo);
        this.libro.write(outputStream);
        this.excelController.descargarArchivo(libro, nombreArchivo);
    }
}

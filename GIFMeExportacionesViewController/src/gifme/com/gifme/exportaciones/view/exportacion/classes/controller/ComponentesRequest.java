package gifme.com.gifme.exportaciones.view.exportacion.classes.controller;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;

public class ComponentesRequest {
    private RichPopup popUpFiltroUsuarios;
    private RichPopup.PopupHints hints = new RichPopup.PopupHints();
    private RichPanelGroupLayout panelGroupFiltros;
    
    public ComponentesRequest() {
        
    }
    
    public void mostrarPopUpFiltros(){
        this.popUpFiltroUsuarios.show(this.hints);
    }

    public void ocultarPopUpFiltros(){
        this.popUpFiltroUsuarios.hide();
    }

    public void setPopUpFiltroUsuarios(RichPopup popUpFiltroUsuarios) {
        this.popUpFiltroUsuarios = popUpFiltroUsuarios;
    }

    public RichPopup getPopUpFiltroUsuarios() {
        return popUpFiltroUsuarios;
    }

    public void setPanelGroupFiltros(RichPanelGroupLayout panelGroupFiltros) {
        this.panelGroupFiltros = panelGroupFiltros;
    }

    public RichPanelGroupLayout getPanelGroupFiltros() {
        return panelGroupFiltros;
    }
}

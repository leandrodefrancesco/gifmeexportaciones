package gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.excel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;

public class ExcelController {
    public ExcelController() {
    }
    
    public void descargarArchivo(Workbook archivo, String nombreArchivo) throws IOException{
        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        archivo.write(outByteStream);
        byte [] outArray = outByteStream.toByteArray();
        this.descargarExcel(outArray, nombreArchivo);
      }
    
    public void descargarExcel(byte[] outArray, String nombreArchivo) throws IOException {
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.setContentType("application/ms-excel");
        response.setContentLength(outArray.length);
        response.setHeader("Expires:", "0");
        response.setHeader("Content-Disposition", "attachment;filename=" + nombreArchivo);
        OutputStream outStream = response.getOutputStream();
        outStream.write(outArray);
        outStream.flush();
    }
}

package gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.csv;


import gifme.com.gifme.exportaciones.view.exportacion.classes.formatos.txt.TxtController;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Csv {
    private TxtController txtController = new TxtController();
    
    public Csv() {
    }
    
    public void exportarCsv(HashMap<String,String> formatoDeExportacion, LinkedHashMap<String,HashMap> mapaDeDatos) 
        throws IOException {
        File archivo = new File(formatoDeExportacion.get("nombreDeArchivo")+".csv");
        PrintStream out = new PrintStream(archivo);
        Iterator iteratorMapaDeDatos = mapaDeDatos.entrySet().iterator();
        while(iteratorMapaDeDatos.hasNext()){
            Map.Entry pair = (Map.Entry)iteratorMapaDeDatos.next();
            HashMap<Integer,String> datos = (HashMap<Integer,String>) pair.getValue();
            if (!datos.equals("null")){
                Iterator iteratorDatos = datos.entrySet().iterator();
                while (iteratorDatos.hasNext()) {
                    Map.Entry elementosObtenidos = (Map.Entry)iteratorDatos.next();
                    HashMap<String,String> valores = (HashMap<String,String>) elementosObtenidos.getValue();
                    Iterator iteradorDatos = valores.entrySet().iterator();
                    while (iteradorDatos.hasNext()){
                        Map.Entry elemento = (Map.Entry)iteradorDatos.next();
                        out.print(elemento.getValue());
                        out.print(formatoDeExportacion.get("Separador"));
                    }
                    out.println();
                }
            }
        }
        out.close();
        this.txtController.descargarArchivoTXT(archivo);
    }
}
